package com.javap.employee.dto;

import java.io.Serializable;

public class ResponseDto implements Serializable {
	
	private String statusCode;
	
	private String status;
	
	private Object data;
	
	private String errorMessage;

	public ResponseDto(String statusCode, String status, Object data, String errorMessage) {
		super();
		this.statusCode = statusCode;
		this.status = status;
		this.data = data;
		this.errorMessage = errorMessage;
	}

}
