package com.javap.employee.service;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.stereotype.Service;



import com.javap.employee.dto.ResponseDto;
import com.javap.employee.entity.LeaveEntity;
import com.javap.employee.repository.LeaveRepository;

@Service
public class LeaveService {
	
	@Autowired
	private LeaveRepository repository;
	
	
	
	public ResponseDto save(LeaveEntity leaveEntity)
	{
		try {
			
			LeaveEntity save = repository.save(leaveEntity);
			System.out.println("something is changed here");
		return	new ResponseDto("200", "success", save, null);
		
		} catch (Exception e) {
			
		 return  new ResponseDto("500", "failure", null, e.getLocalizedMessage());
		}
	
	}
	

	

}
